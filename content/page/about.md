---
title: About us
subtitle: Why you would want to hang out with us
date: 2018-08-14T18:29:00+02:00
bigimg: [
  {src: "img/cape-town.jpeg", desc: "Cape Town"},
  {src: "img/table-mountain.jpeg", desc: "Table Mountain"}
]
comments: false
---

We are an open group interested in collaborating to produce and make use of free (as in freedom) and open-source software.

### Want to know more?

The following sections summarise what we stand for and how you can become part of us.

### Regular Meet-ups

Our Friday fortnightly meet-up especially targets those who are flexible during traditional work hours (students, entrepreneurs, non-profits, freelancers, etc) but anyone is welcome, no matter your skill level or fields of interest!

Refer to our [Facebook event page](https://www.facebook.com/CapeDiff/events) for our next event and add it to your calendar!

### Get involved

We are more than just a fortnightly meet-up. Participants are encouraged to be leaders in our community from the beginning, start discussions, help organise events together and help eachother with side-ventures. Get involved building with us, or start your own thing. The aim is for participants to integrate into the multiple already established communities, various non-profits and other organizations.

### Our friends

Hopefully this list keeps growing but special mention goes to [Codebridge](https://codebridge.org.za/). Our meet-up purposefully fits in between their Thursday evening fortnightly meet-ups and we encourage everyone we can to join us there as well!

### Communication channels

Our main channel of communication is shared at #tech-for-good on [ZATech](https://zatech.github.io/).

[tech-for-good](http://techforgood.org.za/) is a community for those interested in exploring how their tech skills can be used for social good in South Africa. They have good [instructions](http://techforgood.org.za/join) on how to join the channel.

### Why Open-Source?

Together we aim to facilitate learning, while tackling some real South African problems as we go. Sharing what is learned to the wider global community is highly encouraged. Any problems we solve together could potentially have application in a remote community elsewhere in the world. By offering software freely and openly we stand the chance of benefiting more than just ourselves. 

### What does the name, Cape Diff mean?

We maintain publicly accessible git repositories and community pages to facilitate our goals in an open and collaborative way. Cape Diff is the organizational name selected to host our activities under.

Although we established ourselves in the Cape of South Africa, **Cape** transcends geographical location by the following reinterpretations:

  - Create A Positive Environment
  - Community Alliance to Promote Education
  - Care About People Everyday

**Diff** is a commonly used term in tech to describe file changes, however we also have the following reinterpretations:

  - make a Difference
  - Do It For freedom
