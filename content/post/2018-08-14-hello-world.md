---
title: Hello World!
subtitle: Hello Cape Town!
date: 2018-08-14T18:30:00+02:00
bigimg: [{src: "img/cape-town.jpeg", desc: "Cape Town"}]
tags: ["helloworld", "firstpost", "capetown", "beautifulhugo"]
---

Welcome!

First things first, please view our [About page]({{< ref "page/about" >}})

This space can be used to showcase community projects and tutorials, etc. Have a look at [Beautiful Hugo](https://themes.gohugo.io/theme/beautifulhugo/) for examples of how to use some of the functionality.
